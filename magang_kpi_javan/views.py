from django.shortcuts import render, redirect
from .models import User,Role,Review,Kpi
import random, string
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User as UserDb
# Create your views here.

def CreateUser(request):
    ids = ''.join(random.choices(string.digits, k=8))
    user_data = User.objects.all()
    errors = ""
    if request.method == "POST":
        if User.objects.filter(id=ids).exists():
            ids = ''.join(random.choices(string.digits, k=8))
        elif request.POST["nama"] in User.objects.values_list('nama', flat=True):
            errors = "Nama sudah terdaftar, silahkan gunakan nama lainnya."
        elif request.POST["email"] in User.objects.values_list('email', flat=True):
            errors = "Email sudah terdaftar, silahkan gunakan email lainnya."
        elif request.POST["username"] in User.objects.values_list('username', flat=True):
            errors = "Username sudah terdaftar, silahkan gunakan username lainnya."
            print("username")
        elif "admin" in str(request.POST["username"]):
            errors = "Username tidak boleh memiliki kata admin, silahkan gunakan username lainnya."
        else:
            picture = "photo_profile.png"
            User.objects.create(
                    id = ids,
                    nama = request.POST['nama'].title(),
                    role = Role.objects.get(id=request.POST['role']),
                    email = request.POST['email'],
                    divisi = request.POST['divisi'],
                    username = request.POST['username'],
                    password = request.POST['password'],
                    foto = picture,
                    posisi= request.POST['posisi'],
                )
            users = UserDb.objects.create_user(request.POST['username'], request.POST['email'], request.POST['password'])
            return redirect('login')
    context = {
        "user_data":user_data,
        "errors":errors,
    }
    return render(request, 'formRegister.html', context)

